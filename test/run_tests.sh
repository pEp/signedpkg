#!/bin/bash

LDP="$1"
shift

run() {
    ./$1
}

TESTS=$*
i=0

RED='\033[0;31m'
GREEN='\033[0;32m'
C='\033[0m'

for t in $TESTS; do
    if run $t ; then
        printf "\n${GREEN}TEST SUCCEEDED: $t${C}\n\n"
        ((i++))
    else
        printf "\n${RED}TEST FAILED: $t${C}\n\n"
    fi
done

echo $i OF $(echo $TESTS | wc -w) TESTS SUCCEEDED

