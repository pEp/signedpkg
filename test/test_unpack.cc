#include <iostream>
#include "../src/unpack.hh"
#include <cryptopp/files.h>
#include <cryptopp/xed25519.h>

using namespace pEp;

void test_extract_archive()
{
    std::cout << "\n*** test: extract_archive()\n\n";

    SignedPackage::extract_archive("DIST.AD", ".");
    std::cout << "works: DIST.AD extracted\n";

    SignedPackage::extract_archive("DIST.AD", ".");
    std::cout << "works: file archive_file1.testdata extracted\n";

    try {
        SignedPackage::extract_archive("notexist.zip", ".");
        throw std::runtime_error("notexist.zip is not a valid path");
    }
    catch (std::exception& e) {
        std::cout << "works: " << e.what() << "\n";
    }

    try {
        SignedPackage::extract_archive("DIST.AD", "notexist");
        throw std::runtime_error("notexist is not a valid path");
    }
    catch (std::exception& e) {
        std::cout << "works: " << e.what() << "\n";
    }
}

void test_decrypt_distribution_key()
{
    std::cout << "\n*** test: decrypt_distribution_key()\n\n";

    CryptoPP::RSA::PrivateKey provisioning_key;
    SignedPackage::LoadPrivateKey("provisioning_key.der", provisioning_key);

    SignedPackage::decrypt_distribution_key("DIST.KEY", provisioning_key);
}

void test_decrypt_distribution_archive()
{
    std::cout << "\n*** test: decrypt_distribution_archive()\n\n";

    CryptoPP::RSA::PrivateKey provisioning_key;
    SignedPackage::LoadPrivateKey("provisioning_key.der", provisioning_key);

    SignedPackage::decrypt_distribution_archive("DIST.A", "DIST.KEY",
            provisioning_key);
}

void test_extract_deployment_archive()
{
    std::cout << "\n*** test: extract_deployment_archive()\n\n";

    CryptoPP::ed25519PublicKey deployment_key;
    SignedPackage::LoadPublicKey("deployment_key-pub.der", deployment_key);

    CryptoPP::RSA::PrivateKey provisioning_key;
    SignedPackage::LoadPrivateKey("provisioning_key.der", provisioning_key);

    std::filesystem::path tmp_path =
        SignedPackage::extract_deployment_archive(deployment_key,
                provisioning_key, "pEp.ppk");
    std::cout << "extraced to " << tmp_path << "\n";

    std::filesystem::remove_all(tmp_path);
}

void test_install_if_location_empty()
{
    std::cout << "\n*** test: install_if_location_empty()\n\n";

    CryptoPP::ed25519PublicKey deployment_key;
    SignedPackage::LoadPublicKey("deployment_key-pub.der", deployment_key);

    CryptoPP::RSA::PrivateKey provisioning_key;
    SignedPackage::LoadPrivateKey("provisioning_key.der", provisioning_key);

    SignedPackage::install_if_location_empty(deployment_key, provisioning_key,
            "pEp.ppk", "PER_USER_DIRECTORY", "pEp.target");
}

int main()
{
    test_extract_archive();
    test_decrypt_distribution_key();
    test_decrypt_distribution_archive();
    test_extract_deployment_archive();
    test_install_if_location_empty();

    return 0;
}
